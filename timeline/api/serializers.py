from rest_framework.serializers import ModelSerializer, SerializerMethodField
from django.db.models import Max
from main.models import GeologicalEvent, Group


class GroupSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = "__all__"


class GeologicalEventSerializer(ModelSerializer):
    group = GroupSerializer()
    
    class Meta:
        model = GeologicalEvent
        fields = "__all__"