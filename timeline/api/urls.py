from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register("geological_event", views.GeologicalEventsViewset, basename="geological_event")
urlpatterns = router.urls