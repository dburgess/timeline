from rest_framework import viewsets
from rest_framework.exceptions import ParseError

from .serializers import GeologicalEventSerializer
from main.models import GeologicalEvent

class GeologicalEventsViewset(viewsets.ReadOnlyModelViewSet):
    serializer_class = GeologicalEventSerializer
    def get_queryset(self):
        requested_timeline = self.request.query_params.get("timeline")
        if requested_timeline:
            return GeologicalEvent.objects.filter(timeline=requested_timeline)
        else:
            raise ParseError("No timeline specified")