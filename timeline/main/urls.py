from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path("view_timeline/<timeline>", views.view_timeline, name="view_timeline"),
    path("list_timelines", views.list_all_timelines, name="list_all_timelines"),
    path("list_timelines/<user_id>", views.list_user_timelimes, name="list_user_timelines"),
    path("accounts/login/", auth_views.LoginView.as_view(template_name="accounts/login.html"), name="login"),
    path("accounts/logout/", auth_views.LogoutView.as_view(), name="logout"),
]