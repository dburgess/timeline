from django.contrib import admin
from .models import Timeline, Group, GeologicalEvent

# Register your models here.
admin.site.register(Timeline)
admin.site.register(Group)
admin.site.register(GeologicalEvent)