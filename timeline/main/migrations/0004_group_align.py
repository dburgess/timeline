# Generated by Django 4.2.6 on 2023-10-28 13:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_alter_geologicalevent_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='align',
            field=models.BooleanField(default=False),
        ),
    ]
