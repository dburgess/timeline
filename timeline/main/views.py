from django.template.response import TemplateResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from .models import Timeline

def view_timeline(request, timeline):
    requested_timeline = get_object_or_404(Timeline, pk=timeline)
    return TemplateResponse(request, "main/view_timeline.html", {
        "user": request.user,
        "timeline": requested_timeline,
    })

def list_all_timelines(request):
    timelines = Timeline.objects.filter(public=True)
    return TemplateResponse(request, "main/list_timelines.html", {
        "user": request.user,
        "timelines": timelines,
    })

@login_required
def list_user_timelimes(request, user_id):
    User = get_user_model()
    viewing_user = get_object_or_404(User, pk=user_id)
    if request.user.id != viewing_user.id:
        raise PermissionDenied()
    
    user_timelines = Timeline.objects.filter(user=viewing_user.id)
    
    return TemplateResponse(request, "main/list_timelines.html", {
        "user": request.user,
        "viewing_user": viewing_user.username,
        "timelines": user_timelines,
    })       
