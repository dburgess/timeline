from django.db import models
from colorfield.fields import ColorField
from django.conf import settings

class Timeline(models.Model):
    def __str__(self):
        return self.name
    class TimelineType(models.TextChoices):
        GEOLOGICAL = "GEOL", "Geological"
        CONVENTIONAL = "CONV", "Conventional"
    name = models.CharField(max_length=200, null=False, blank=False)
    description = models.CharField(max_length=400, null=True, blank=True)
    type = models.CharField(max_length=4, choices=TimelineType.choices, null=False, blank=False)
    public = models.BooleanField(null=False, blank=False, default=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, blank=False, editable=False, on_delete=models.PROTECT)

class Group(models.Model):
    def __str__(self):
        return self.name
    name = models.CharField(max_length=200, null=False, blank=False)
    colour = ColorField(default="#EEEEEE")
    align = models.BooleanField(default=False, null=False, blank=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, blank=False, editable=False, on_delete=models.PROTECT)

class GeologicalEvent(models.Model):
    def __str__(self):
        return self.name
    
    class GeologicalEventType(models.TextChoices):
        POINT = "POINT", "Point"
        PERIOD = "PERID", "Period"
        
    name = models.CharField(max_length=200, null=False, blank=False)
    timeline = models.ForeignKey(Timeline, on_delete=models.PROTECT, null=False, blank=False)
    type = models.CharField(max_length=5, choices=GeologicalEventType.choices, null=False, blank=False)
    start_ma = models.FloatField(null=False, blank=False)
    start_err_lb_ma = models.FloatField(null=True, blank=True)
    start_err_ub_ma = models.FloatField(null=True, blank=True)
    end_ma = models.FloatField(null=True, blank=True)
    end_err_lb_ma = models.FloatField(null=True, blank=True)
    end_err_ub_ma = models.FloatField(null=True, blank=True)
    group = models.ForeignKey(Group, on_delete=models.PROTECT, null=True, blank=True)
    reference = models.CharField(max_length=500)
    reference_link = models.CharField(max_length=500)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, blank=False, editable=False, on_delete=models.PROTECT)